
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class NewUserServlet
 */
@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*フォワード(サーブレットからjspを利用。この場合newUser.jspを利用。)
		 *これは、新規登録画面を表示をするための処理。
		 */
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得()
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String date = request.getParameter("date");

		/** テーブルに該当のデータが見つからなかった場合**/
		boolean isError = false;

		//既に登録されているログインIDが入力された場合(
		UserDao userId = new UserDao();
		boolean isUserId = userId.isExistLoginId(loginId);

		if (isUserId) {
			isError = true;

		}

		// すべて入力されていない場合
		if (loginId.equals("") || password.equals("") || name.equals("") || date.equals("")) {
			isError = true;
		}

		//パスワードが一致しない場合
		if (!password.equals(password2)) {
			isError = true;
		}

		if (isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// 新規ユーザjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);

		} else {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行(エラーがないかチェックが先なのでエラーの条件分岐を先に書く）
			UserDao userDao = new UserDao();
			userDao.InsertUser(loginId, password, name, date);

			/** テーブルに該当のデータが見つかった場合 **/
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}
	}
}
