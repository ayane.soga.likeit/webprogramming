
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		UserDao userDetails = new UserDao();
		User user = userDetails.UserDetails(id);

		request.setAttribute("userDetails", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得()loginIdとpasswordはloginIdとpasswordは、formのnameで決めた名前
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String date = request.getParameter("date");

		/** テーブルに該当のデータが見つからなかった場合(ログイン失敗時) **/

		if (password.equals("") || password2.equals("")) {

		}

		boolean isError = false;

		// すべて入力されていない場合
		if (name.equals("") || date.equals("")) {
			isError = true;
		}

		//パスワードが一致しない場合
		if (!password.equals(password2)) {
			isError = true;
		}

		if (isError) {
			request.setAttribute("errMsg", "入力された内容は正しくされません");

			// 更新ユーザjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		} else {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行(エラーがないかチェックが先なのでエラーの条件分岐を先に書く）
			UserDao userDao = new UserDao();
			userDao.UpdateUser(password, name, date, id);

			/** テーブルに該当のデータが見つかった場合 **/
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}
	}
}
