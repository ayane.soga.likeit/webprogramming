package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {

	//findByLoginInfoは、今回ログインのデータを探すためのメソッド
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			//確認済みのSQL
			String sql = "SELECT*FROM user WHERE login_id=? and password=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			/*rs.next()は、呼び出すたびに次のレコードに進むようになっている。
			 *今回は、1つのレコードを呼び出すため１回の処理だけ
			 */

			//ログイン時の失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	* 全てのユーザ情報を取得する
	* @return
	*/
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			//rsにはsqlの内容を入れる
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			//rsには、レコードの内容が詰まっている
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/*
	 * データを追加する(
	 */
	public void InsertUser(String loginId, String password, String name, String date) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			//INSERT文を準備(now関数を直接書くと現在時刻が登録される）
			String sql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now()) ";

			//INSERTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, date);
			int result = pStmt.executeUpdate();

			// 追加された行数を出力
			System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	//既に登録されているログインIDが入力された場合(データの登録は行わない)
	//ログインIDがみつかった場合はtrue、みつからなかった場合は、false
	public boolean isExistLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			//確認済みのSQL
			String sql = "SELECT*FROM user WHERE login_id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			//ログイン時の失敗時の処理
			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return false;
	}

	//ユーザ詳細のデータ
	public User UserDetails(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			//確認済みのSQL
			String sql = "SELECT*FROM user WHERE id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			//ログイン時の失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//成功時の処理
			int Id = rs.getInt("id");
			String loginId2 = rs.getString("login_id");
			String name2 = rs.getString("name");
			String date = rs.getString("birth_date");
			String createDate2 = rs.getString("create_date");
			String updateDate2 = rs.getString("update_date");
			return new User(Id, loginId2, name2, date, createDate2, updateDate2);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//データを更新する
	public void UpdateUser(String password, String name, String date, String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			//UPDATE文を準備
			String sql = "UPDATE user SET password=?,name=?,birth_date=? WHERE id=?";

			//UPDATEを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, date);
			pStmt.setString(4, id);
			int result = pStmt.executeUpdate();

			// 追加された行数を出力
			System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	//データの削除
	public void UserDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			//確認済みのSQL
			String sql = "DELETE FROM user WHERE id=?";

			//DELETEを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			int result = pStmt.executeUpdate();

			// 追加された行数を出力
			System.out.println(result);
			pStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();

				} finally {
			// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
				}
			}
		}
	}
}