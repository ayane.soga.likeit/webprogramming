package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBmanager {
	  final private static String URL = "jdbc:mysql://localhost/";
	    final private static String DB_NAME = "userList";
	    final private static String PARAMETERS = "?useUnicode=true&characterEncoding=utf8";
	    final private static String USER = "root";
	    final private static String PASS = "password";
	    /**DB接続への接続のコネクションを作成するメソッドを持ったクラス。
	     * Daoと同じパッケージに所属させて、コネクションを生成する際にメソッドを呼び出す。
	 	 * DBへ接続するコネクションを返す
	 	 */
	    public static Connection getConnection() {
	    	Connection con = null;
	        try {
	        	Class.forName("com.mysql.jdbc.Driver");
	            con = DriverManager.getConnection(URL+DB_NAME+PARAMETERS,USER,PASS);
	    	} catch (SQLException | ClassNotFoundException e) {
	            e.printStackTrace();
	    	}
	        return con;
		}
	}

