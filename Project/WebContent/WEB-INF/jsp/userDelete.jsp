<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action="/UserManagement/UserDeleteServlet" method="post">
<input type="hidden" name=id  value="${userDetails.id}">
<div class="container">
	<nav class="navbar-dark bg-dark">
	<ul class="nav justify-content-end">

  	<li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"><font size="5"color="white">${userInfo.name}</font></a>
  	</li>
  	<li class="nav-item">
    <a class="nav-link" href="LoginServlet"style="text-decoration: underline"><font size="5">ログアウト</font></a>
  	</li>
	</ul>
	</nav>
	<h1 align="center">ユーザ削除確認</h1>
	<font size="5">ログインID:${userDetails.loginId}<br>
	を本当に削除してよろしいでしょうか。<br>
	</font>

	<div align="center">
	<button class="button1" type="button">キャンセル</button>
	<button class="button2" type="submit">OK</button>
	</div>
	</div>
	</form>
</body>
</html>