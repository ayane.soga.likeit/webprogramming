<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<h1 align="center">ログイン画面</h1>
	<div class="container">
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	</div>

	<form action="/UserManagement/LoginServlet" method="post">
		<div class="login-area">
			<div class="form-group row">
				<label for="inoputLogin" class="col-sm-4 col-form-label">ログインID</label>
				<div class="col-sm-8">
					<input type="text" name=loginId class="form-control"
						id="inputLogin">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-4 col-form-label">パスワード</label>
				<div class="col-sm-8">
					<input type="password" name=password class="form-control"
						id="inputPassword">
				</div>
			</div>
		</div>
		<div align="center">
			<button class="button1" type="submit">ログイン</button>
		</div>
	</form>
</body>
</html>