<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
	<nav class="navbar-dark bg-dark">
	<ul class="nav justify-content-end">

  	<li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"><font size="5"color="white">${userInfo.name}</font></a>
  	</li>
  	<li class="nav-item">
    <a class="nav-link" href="LoginServlet"style="text-decoration: underline"><font size="5">ログアウト</font></a>
  	</li>
	</ul>
	</nav>

	<h1 align="center">ユーザ情報詳細参照</h1>

	<div class="login-area">
	<div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">ログインID</label>
    <div class="col-sm-8">
	${userDetails.loginId}
    </div>
    </div>
    <div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">ユーザ名</label>
    <div class="col-sm-8">
	${userDetails.name}
    </div>
    </div>
    <div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">生年月日</label>
    <div class="col-sm-8">
	${userDetails.birthDate }
    </div>
    </div>
    <div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">登録日時</label>
    <div class="col-sm-8">
	${userDetails.createDate }
    </div>
    </div>
     <div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">更新日時</label>
    <div class="col-sm-8">
	${userDetails.updateDate}
    </div>
    </div>
    </div>
	<div align="left"><a href="UserListServlet" style="text-decoration: underline"><font size="5">戻る</font></a></div>
	</div>
</body>
</html>