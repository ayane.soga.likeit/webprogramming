<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
		<nav class="navbar-dark bg-dark">
			<ul class="nav justify-content-end">
				<li class="nav-item"><a class="nav-link disabled" href="#"
					tabindex="-1" aria-disabled="true"><font size="5" color="white">${userInfo.name}</font></a>
				</li>
				<li class="nav-item"><a class="nav-link" href="LoginServlet"
					style="text-decoration: underline"><font size="5">ログアウト</font></a>
				</li>
			</ul>
		</nav>

		<h1 align="center">ユーザ一覧</h1>
		<div align="right">
			<a href="NewUserServlet" style="text-decoration: underline"><font size="5">新規登録</font></a>
			</div>



		<div class="form-group row">
			<label for="inputLogin" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="inputLogin">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputName" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="inputName">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputdate" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-8 row">
				<input type="text" class="col-sm-5 form-control" id="inputdate"
					placeholder="年/月/日">
				<div class="col-sm-1">～</div>
				<input type="text" class="col-sm-5 form-control" id="inputdate"
					placeholder="年/月/日">
			</div>
		</div>

		<div align="right">
			<button class="button1" type="submit">検索</button>
		</div>
		<hr Align="center" Width="1100">
		<table class="table table-bordered">
			<thead>

				<tr>
					<th class="table-active">ログインID</th>
					<th class="table-active">ユーザ名</th>
					<th class="table-active">生年月日</th>
					<th class="table-active"></th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="user" items="${userList}" >
				<tr>
					<td valign="top">${user.loginId}</td>
					<td valign="middle">${user.name}</td>
					<td valign="middle">${user.birthDate}</td>
					<td>

						<a href="UserDetailsServlet?id=${user.id }"><button type="button" class="btn btn-primary">詳細</button></a>
						<a href="UserUpdateServlet?id=${user.id }"><button type="button" class="btn btn-success">更新</button></a>
						<a href="UserDeleteServlet?id=${user.id }"><button type="button" class="btn btn-danger">削除</button></a>

					</td>
				</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>