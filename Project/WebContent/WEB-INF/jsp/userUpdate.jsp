<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">
	<nav class="navbar-dark bg-dark">
	<ul class="nav justify-content-end">

  	<li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"><font size="5"color="white">${userInfo.name}</font></a>
  	</li>
  	<li class="nav-item">
    <a class="nav-link" href="LoginServlet"style="text-decoration: underline"><font size="5">ログアウト</font></a>
  	</li>
	</ul>
	</nav>
	<h1 align="center">ユーザ情報更新</h1>

	<form action="/UserManagement/UserUpdateServlet" method="post">
	<input type="hidden" name=id  value="${userDetails.id}">
	<div class="login-area">
	<div class="form-group row">
    <label for="staticEmail" class="col-sm-4 col-form-label">ログインID</label>
    <div class="col-sm-8">
    ${userDetails.loginId}
    </div>
    </div>
	<div class="form-group row">
	<label for="inputName" class="col-sm-4 col-form-label">パスワード</label>
	<div class="col-sm-8">
	<input type="password" class="form-control" name=password id="inputPassword">
	</div>
	</div>
	<div class="form-group row">
	<label for="inputdate" class="col-sm-4 col-form-label">パスワード(確認)</label>
	<div class="col-sm-8">
	<input type="text" class="form-control" name=password2 id="inputPassword">
	</div>
	</div>
	<div class="form-group row">
	<label for="inputdate" class="col-sm-4 col-form-label">ユーザ名</label>
	<div class="col-sm-8">
	<input type="text" class="form-control" name=name id="inputName" value="${userDetails.name}">
	</div>
	</div>
	<div class="form-group row">
	<label for="inputdate" class="col-sm-4 col-form-label">生年月日</label>
	<div class="col-sm-8">
	<input type="date" class="form-control" name=date id="inputDate"value="${userDetails.birthDate}">
	</div>
	</div>
	</div>
	<div align="center"><button class="button1" type="submit">更新</button></div>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	</form>
	<div align="left"><a href="UserListServlet" style="text-decoration: underline"><font size="5">戻る</font></a></div>
	</div>
</body>
</html>